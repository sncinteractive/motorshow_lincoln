package com.sgcom.specstand.lincoln.component.detail.fragment.mkx;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.MKXActivity;
import com.sgcom.specstand.lincoln.helper.RecyclingBitmapDrawable;
import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 18..
 */

public class MkxExteriorFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "MkxExteriorFragment";

	private View mView;

	private ImageView mExteriorImageView;
	private TextView mExteriorColorName;
	private View mExteriorColorSelector;

	private ExteriorType mPrevSelectedType;

	private enum ExteriorType {
		BV          (R.id.BT_EX_MKX_BV, R.drawable.mkx_ex_blkv, "Black Velvet", 378, 389),
		IS        (R.id.BT_EX_MKX_IS, R.drawable.mkx_ex_islv, "Ingot Silver", 432, 439),
		PGM         (R.id.BT_EX_MKX_PGM, R.drawable.mkx_ex_pagd, "Palladium Gold\nMetallic", 472, 489),
		MGM         (R.id.BT_EX_MKX_MGM, R.drawable.mkx_ex_mgnt, "Magnetic Gray\nMetallic", 522, 539),
		WPMT       (R.id.BT_EX_MKX_WPMT, R.drawable.mkx_ex_whpl, "White Platinum\nMetallic Tri-coat", 567, 589),
		LUX        (R.id.BT_EX_MKX_LUX, R.drawable.mkx_ex_luxe, "Luxe", 654, 639),
		BVMTC        (R.id.BT_EX_MKX_BVMTC, R.drawable.mkx_ex_brvt, "Burgundy Velvet\nMetallic Tinted Clearcoat", 642, 689),
		MSBM         (R.id.BT_EX_MKX_MSBM, R.drawable.mkx_ex_mdns, "Midnight Sapphire\nBlue Metallic", 708, 739),
		RRMTC        (R.id.BT_EX_MKX_RRMTC, R.drawable.mkx_ex_rbrd, "Ruby Red Metallic\nTinted Clearcoat", 763, 789),
		DBM         (R.id.BT_EX_MKX_DBM, R.drawable.mkx_ex_dblk, "Diamind Black\nMetallic", 826, 839)
		;

		int typeId;
		int imageResId;
		String colorName;
		int colorNameMarginLeft;
		int colorSelectorMarginLeft;

		private ExteriorType(int typeId, int imageResId, String colorName, int colorNameMarginLeft, int colorSelectorMarginLeft) {
			this.typeId = typeId;
			this.imageResId = imageResId;
			this.colorName = colorName;
			this.colorNameMarginLeft = colorNameMarginLeft;
			this.colorSelectorMarginLeft = colorSelectorMarginLeft;
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_mkx_exterior, container, false);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initLayout() {
		try {
			mExteriorImageView = (ImageView) mView.findViewById(R.id.EX_IMAGE_VIEWER);
			mExteriorColorName = (TextView) mView.findViewById(R.id.EX_COLOR_NAME);
			mExteriorColorSelector = (View) mView.findViewById(R.id.EX_COLOR_SELECTOR);

			SGUtils.setFontToView(((MKXActivity)getActivity()).mFontProximaNovaRegular, mExteriorColorName);

			for (ExteriorType type : ExteriorType.values()) {
				View view = mView.findViewById(type.typeId);
				view.setOnClickListener(this);
			}

			changeExteriorImage(ExteriorType.BV);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeExteriorImage(ExteriorType extType) {
		try {
			if(mPrevSelectedType == null) {
				Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), extType.imageResId);
				RecyclingBitmapDrawable currentExtImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

				mExteriorImageView.setImageDrawable(currentExtImage);
				mExteriorColorName.setText(extType.colorName);
			} else {
				TransitionDrawable transitionDrawable = getBitmapDrawableFromType(extType);

				mExteriorImageView.setImageDrawable(transitionDrawable);
				mExteriorColorName.setText(extType.colorName);
			}

			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, SGUtils.dp(getActivity(), 35));
			llp.setMargins(SGUtils.dp(getActivity(), extType.colorNameMarginLeft), 0, 0, 0); // llp.setMargins(left, top, right, bottom);
			mExteriorColorName.setLayoutParams(llp);

			LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(SGUtils.dp(getActivity(), 52), SGUtils.dp(getActivity(), 4));
			llp2.setMargins(SGUtils.dp(getActivity(), extType.colorSelectorMarginLeft), 0, 0, SGUtils.dp(getActivity(), 2)); // llp.setMargins(left, top, right, bottom);
			mExteriorColorSelector.setLayoutParams(llp2);

			mPrevSelectedType = extType;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private TransitionDrawable getBitmapDrawableFromType(ExteriorType extType) {
		try {
			Bitmap prevBitmap = BitmapFactory.decodeResource(getResources(), mPrevSelectedType.imageResId);
			RecyclingBitmapDrawable preExtImage = new RecyclingBitmapDrawable(getResources(), prevBitmap);

			Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), extType.imageResId);
			RecyclingBitmapDrawable currentExtImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

			Drawable[] layers = new Drawable[2];
			layers[0] = new BitmapDrawable(getResources(), preExtImage.getBitmap());
			layers[1] = new BitmapDrawable(getResources(), currentExtImage.getBitmap());

			TransitionDrawable transitionDrawable = new TransitionDrawable(layers);
			transitionDrawable.startTransition(700);

			return transitionDrawable;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
				case R.id.BT_EX_MKX_BV :
					changeExteriorImage(ExteriorType.BV);
					break;
				case R.id.BT_EX_MKX_IS :
					changeExteriorImage(ExteriorType.IS);
					break;
				case R.id.BT_EX_MKX_PGM :
					changeExteriorImage(ExteriorType.PGM);
					break;
				case R.id.BT_EX_MKX_MGM :
					changeExteriorImage(ExteriorType.MGM);
					break;
				case R.id.BT_EX_MKX_WPMT :
					changeExteriorImage(ExteriorType.WPMT);
					break;
				case R.id.BT_EX_MKX_LUX :
					changeExteriorImage(ExteriorType.LUX);
					break;
				case R.id.BT_EX_MKX_BVMTC :
					changeExteriorImage(ExteriorType.BVMTC);
					break;
				case R.id.BT_EX_MKX_MSBM :
					changeExteriorImage(ExteriorType.MSBM);
					break;
				case R.id.BT_EX_MKX_RRMTC :
					changeExteriorImage(ExteriorType.RRMTC);
					break;
				case R.id.BT_EX_MKX_DBM :
					changeExteriorImage(ExteriorType.DBM);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
