package com.sgcom.specstand.lincoln.helper;

import android.content.Context;
import android.content.Intent;

import com.sgcom.specstand.lincoln.component.detail.ContinentalActivity;
import com.sgcom.specstand.lincoln.component.detail.MKCActivity;
import com.sgcom.specstand.lincoln.component.detail.MKXActivity;
import com.sgcom.specstand.lincoln.component.detail.MKZActivity;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class PageFactory {
	private static final String TAG = "PageFactory";

	public static Intent makeContinentalPage(Context context) {
		try {
			Intent intent = new Intent(context, ContinentalActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			return intent;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Intent makeMKXPage(Context context) {
		try {
			Intent intent = new Intent(context, MKXActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			return intent;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Intent makeMKCPage(Context context) {
		try {
			Intent intent = new Intent(context, MKCActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			return intent;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Intent makeMKZPage(Context context) {
		try {
			Intent intent = new Intent(context, MKZActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			return intent;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
