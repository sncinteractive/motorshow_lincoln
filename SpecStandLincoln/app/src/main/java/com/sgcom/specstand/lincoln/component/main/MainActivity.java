package com.sgcom.specstand.lincoln.component.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.helper.SGUtils;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);

			setContentView(R.layout.activity_main);

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initLayout() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		try {
			if(hasFocus) {
				super.onWindowFocusChanged(hasFocus);
				SGUtils.hideAllSystemUI(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		try {
			SGUtils.hideAllSystemUI(this);
			super.onResume();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		try {
//			super.onBackPressed();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
