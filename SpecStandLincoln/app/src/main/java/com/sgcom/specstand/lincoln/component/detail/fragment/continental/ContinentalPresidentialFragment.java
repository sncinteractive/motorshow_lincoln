package com.sgcom.specstand.lincoln.component.detail.fragment.continental;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.adapter.CustomPagerAdapter;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class ContinentalPresidentialFragment extends Fragment implements View.OnClickListener, View.OnTouchListener, ViewPager.OnPageChangeListener {
	private static final String TAG = "ContinentalPresidentialFragment";

	private View mView;

	private ViewPager mPager;
	private ImageView mBtnUp;
	private ImageView mBtnDown;
	private ImageView mBtnLeft;
	private ImageView mBtnRight;

	int[][] mResources = {
		new int[] { R.drawable.continental_presidential_rhapsody_1, R.drawable.continental_presidential_rhapsody_2, R.drawable.continental_presidential_rhapsody_3 },
		new int[] { R.drawable.continental_presidential_chalet_1, R.drawable.continental_presidential_chalet_2, R.drawable.continental_presidential_chalet_3 },
		new int[] { R.drawable.continental_presidential_thoroughbred_1, R.drawable.continental_presidential_thoroughbred_2, R.drawable.continental_presidential_thoroughbred_3 }
	};

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_continental_presidential, container, false);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initLayout() {
		try {
			mPager = (ViewPager) mView.findViewById(R.id.VIEW_PAGER);

			mBtnUp = (ImageView) mView.findViewById(R.id.BT_UP);
			mBtnDown = (ImageView) mView.findViewById(R.id.BT_DOWN);
			mBtnLeft = (ImageView) mView.findViewById(R.id.BT_LEFT);
			mBtnRight = (ImageView) mView.findViewById(R.id.BT_RIGHT);

			mBtnUp.setSelected(false);
			mBtnDown.setSelected(true);
			mBtnLeft.setSelected(false);
			mBtnRight.setSelected(true);

			mBtnLeft.setOnClickListener(this);
			mBtnRight.setOnClickListener(this);

			mBtnUp.setOnTouchListener(this);
			mBtnDown.setOnTouchListener(this);

			CustomPagerAdapter adapter = new CustomPagerAdapter(getActivity(), mPager, mResources);

			mPager.setAdapter(adapter);

			mPager.addOnPageChangeListener(this);

			changeButtonStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeButtonStatus() {
		try {
			int current = mPager.getCurrentItem();
			if(current <= 0) {
				mBtnLeft.setSelected(false);
			} else {
				mBtnLeft.setSelected(true);
			}

			if(current < mPager.getAdapter().getCount() - 1) {
				mBtnRight.setSelected(true);
			} else {
				mBtnRight.setSelected(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
				case R.id.BT_LEFT :
					mPager.setCurrentItem(mPager.getCurrentItem() - 1, true);
					break;
				case R.id.BT_RIGHT :
					mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	}

	@Override
	public void onPageSelected(int position) {
		try {
			changeButtonStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		try {
			if(mPager != null) {
				mPager.clearOnPageChangeListeners();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
