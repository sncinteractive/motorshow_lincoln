package com.sgcom.specstand.lincoln.helper;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by youngmincho on 2017. 3. 18..
 */

public class RecyclingImageView extends ImageView {
	private Matrix matrix = new Matrix();
	private Drawable previousDrawable;

	public RecyclingImageView(Context context) {
		super(context);
	}

	public RecyclingImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RecyclingImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	/**
	 * @see android.widget.ImageView#onDetachedFromWindow()
	 */
	@Override
	protected void onDetachedFromWindow() {
		// This has been detached from Window, so clear the drawable
		setImageDrawable(null);

		super.onDetachedFromWindow();
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
	                        int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		Drawable drawable = getDrawable();
		if (drawable != null && previousDrawable != drawable
				&& ScaleType.MATRIX == getScaleType()) {
			float scale;
			float dx = 0;

			int dwidth = drawable.getIntrinsicWidth();
			int dheight = drawable.getIntrinsicHeight();

			int vwidth = getWidth() - getPaddingLeft() - getPaddingRight();
			int vheight = getHeight() - getPaddingTop() - getPaddingBottom();

			if (dwidth * vheight > vwidth * dheight) {
				scale = (float) vheight / (float) dheight;
				dx = (vwidth - dwidth * scale) * 0.5f;
			} else {
				scale = (float) vwidth / (float) dwidth;
			}

			matrix.setScale(scale, scale);
			matrix.postTranslate((int) (dx + 0.5f), 0);

			setImageMatrix(matrix);
		}
	}

	/**
	 * @see android.widget.ImageView#setImageDrawable(android.graphics.drawable.Drawable)
	 */
	@Override
	public void setImageDrawable(Drawable drawable) {
		// Keep hold of previous Drawable
		previousDrawable = getDrawable();

		// Call super to set new Drawable
		super.setImageDrawable(drawable);

		// Notify new Drawable that it is being displayed
		notifyDrawable(drawable, true);

		// Notify old Drawable so it is no longer being displayed
		notifyDrawable(previousDrawable, false);
	}

	@Override
	public void setImageResource(int resId) {
		throw new UnsupportedOperationException("don't use this API!");
	}

	/**
	 * Notifies the drawable that it's displayed state has changed.
	 *
	 * @param drawable
	 * @param isDisplayed
	 */
	private static void notifyDrawable(Drawable drawable,
	                                   final boolean isDisplayed) {
		if (drawable instanceof RecyclingBitmapDrawable) {
			// The drawable is a CountingBitmapDrawable, so notify it
			((RecyclingBitmapDrawable) drawable).setIsDisplayed(isDisplayed);
		} else if (drawable instanceof LayerDrawable) {
			// The drawable is a LayerDrawable, so recurse on each layer
			LayerDrawable layerDrawable = (LayerDrawable) drawable;
			for (int i = 0, z = layerDrawable.getNumberOfLayers(); i < z; i++) {
				notifyDrawable(layerDrawable.getDrawable(i), isDisplayed);
			}
		}
	}

}