package com.sgcom.specstand.lincoln.component.detail.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.view.SGScrollView;
import com.sgcom.specstand.lincoln.helper.RecyclingImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class CustomPagerAdapter extends PagerAdapter {
	private static final String TAG = "CustomPagerAdapter";

	Context mContext;
	ViewPager mPager;
	LayoutInflater mLayoutInflater;

	int[][] mResources;

	public CustomPagerAdapter(Context context, ViewPager pager, int[][] resources) {
		mContext = context;
		mPager = pager;
		mResources = resources.clone();
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mResources.length;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((SGScrollView) object);
	}

	@Override
	public Object instantiateItem(final ViewGroup container, final int position) {
		View itemView = null;

		try {
			itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

			SGScrollView scrollView = (SGScrollView) itemView.findViewById(R.id.SCROLL_IMAGE_VIEWER);
			scrollView.setContext(this.mContext);
			RecyclingImageView imageView1 = (RecyclingImageView) itemView.findViewById(R.id.IMAGE_VIEWER1);
			RecyclingImageView imageView2 = (RecyclingImageView) itemView.findViewById(R.id.IMAGE_VIEWER2);
			RecyclingImageView imageView3 = (RecyclingImageView) itemView.findViewById(R.id.IMAGE_VIEWER3);

			Button btnRhapsody = (Button) itemView.findViewById(R.id.BT_RHAPSODY);
			Button btnChalet = (Button) itemView.findViewById(R.id.BT_CHALET);
			Button btnThoroughbred = (Button) itemView.findViewById(R.id.BT_THOROUGHBRED);
			btnRhapsody.setOnClickListener(null);
			btnChalet.setOnClickListener(null);
			btnThoroughbred.setOnClickListener(null);

			Picasso.with(mContext).load(mResources[position][0]).into(imageView1);
			Picasso.with(mContext).load(mResources[position][1]).into(imageView2);
			Picasso.with(mContext).load(mResources[position][2]).into(imageView3);

//			Bitmap bitmap1 = BitmapFactory.decodeResource(mContext.getResources(), mResources[position][0]);
//			Bitmap bitmap2 = BitmapFactory.decodeResource(mContext.getResources(), mResources[position][1]);
//			Bitmap bitmap3 = BitmapFactory.decodeResource(mContext.getResources(), mResources[position][2]);
//
//			imageView1.setImageDrawable(new RecyclingBitmapDrawable(mContext.getResources(), bitmap1));
//			imageView2.setImageDrawable(new RecyclingBitmapDrawable(mContext.getResources(), bitmap2));
//			imageView3.setImageDrawable(new RecyclingBitmapDrawable(mContext.getResources(), bitmap3));

			if(position == 0) {
				btnRhapsody.setSelected(true);
				btnChalet.setSelected(false);
				btnThoroughbred.setSelected(false);
			} else if(position == 1) {
				btnRhapsody.setSelected(false);
				btnChalet.setSelected(true);
				btnThoroughbred.setSelected(false);
			} else if(position == 2) {
				btnRhapsody.setSelected(false);
				btnChalet.setSelected(false);
				btnThoroughbred.setSelected(true);
			}

			btnRhapsody.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mPager.setCurrentItem(0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnChalet.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mPager.setCurrentItem(1);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnThoroughbred.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mPager.setCurrentItem(2);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			container.addView(itemView);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		try {
			container.removeView((SGScrollView) object);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
