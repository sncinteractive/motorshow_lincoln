package com.sgcom.specstand.lincoln.component.main;

import android.app.Activity;
import android.graphics.Typeface;

import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class SGActivity extends Activity {
	private static final String TAG = "SGActivity";

	public Typeface mFontMillerBannerLight;
	public Typeface mFontProximaNovaRegular;

	public void initFonts() {
		try {
			mFontMillerBannerLight = Typeface.createFromAsset(getAssets(), "fonts/LincolnMillerB-Light.otf");
			mFontProximaNovaRegular = Typeface.createFromAsset(getAssets(), "fonts/Lincoln-ProximaNova-Reg.otf");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		try {
			if(hasFocus) {
				super.onWindowFocusChanged(hasFocus);
				SGUtils.hideAllSystemUI(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		try {
			SGUtils.hideAllSystemUI(this);
			super.onResume();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void onBackPressed() {
		try {
			super.onBackPressed();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
