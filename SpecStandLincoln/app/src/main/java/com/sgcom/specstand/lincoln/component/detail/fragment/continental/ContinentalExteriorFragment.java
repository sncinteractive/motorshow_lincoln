package com.sgcom.specstand.lincoln.component.detail.fragment.continental;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.ContinentalActivity;
import com.sgcom.specstand.lincoln.helper.RecyclingBitmapDrawable;
import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 17..
 */

public class ContinentalExteriorFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "ContinentalExteriorFragment";

	private View mView;

	private ImageView mExteriorImageView;
	private TextView mExteriorColorType;
	private TextView mExteriorColorName;
	private View mExteriorColorSelector;

	private ExteriorType mPrevSelectedType;

	private enum ExteriorType {
		RES_BV          (R.id.BT_EX_CONTINENTAL_RESERVE_BV, R.drawable.continental_ex_reserve_blkv, R.string.continental_ex_color_type_reserve, "Black Velvet", 247, 258),
		RES_PWGM        (R.id.BT_EX_CONTINENTAL_RESERVE_PWGM, R.drawable.continental_ex_reserve_pagd, R.string.continental_ex_color_type_reserve, "Palladium White\nGold Metallic", 288, 309),
		RES_MGM         (R.id.BT_EX_CONTINENTAL_RESERVE_MGM, R.drawable.continental_ex_reserve_mgnt, R.string.continental_ex_color_type_reserve, "Magnetic Gray\nMetallic", 340, 359),
		RES_JGM         (R.id.BT_EX_CONTINENTAL_RESERVE_JGM, R.drawable.continental_ex_reserve_jade, R.string.continental_ex_color_type_reserve, "Jade Green\nMetallic", 400, 409),
		RES_BVMTC       (R.id.BT_EX_CONTINENTAL_RESERVE_BVMTC, R.drawable.continental_ex_reserve_brvt, R.string.continental_ex_color_type_reserve, "Burgundy Velvet\nMetallic Tinted Clearcoat", 410, 459),
		RES_RRMTC       (R.id.BT_EX_CONTINENTAL_RESERVE_RRMTC, R.drawable.continental_ex_reserve_rbrd, R.string.continental_ex_color_type_reserve, "Ruby Red Metallic\nTinted Clearcoat", 482, 509),
		RES_MSBM        (R.id.BT_EX_CONTINENTAL_RESERVE_MSBM, R.drawable.continental_ex_reserve_mdns, R.string.continental_ex_color_type_reserve, "Midnight Sapphire\nBlue Metallic", 532, 560),
		COM_ISM         (R.id.BT_EX_CONTINENTAL_COMMON_ISM, R.drawable.continental_ex_presidential_islv, R.string.continental_ex_color_type_common, "Ingot Silver\nMetallic", 655, 665),
		COM_WPMT        (R.id.BT_EX_CONTINENTAL_COMMON_WPMT, R.drawable.continental_ex_presidential_whpl, R.string.continental_ex_color_type_common, "White Platinum\nMetallic Tri-coat", 693, 715),
		COM_DBM         (R.id.BT_EX_CONTINENTAL_COMMON_DBM, R.drawable.continental_ex_presidential_dblkm, R.string.continental_ex_color_type_common, "Diamind Black\nMetallic", 751, 765),
		PRE_RBM         (R.id.BT_EX_CONTINENTAL_PRESIDENTIAL_RBM, R.drawable.continental_ex_presidential_rbmetal, R.string.continental_ex_color_type_presidential, "Rhapsody Blue\nMetallic", 851, 870),
		PRE_CECPM       (R.id.BT_EX_CONTINENTAL_PRESIDENTIAL_CECPM, R.drawable.continental_ex_presidential_cecpmetal, R.string.continental_ex_color_type_presidential, "Chroma Elite\nCopper Premium Metallic", 871, 920),
		PRE_CCDGPM      (R.id.BT_EX_CONTINENTAL_PRESIDENTIAL_CCDGPM, R.drawable.continental_ex_presidential_ccdgpmetal, R.string.continental_ex_color_type_presidential, "Chroma Caviar\nDark Gray Premium Metallic", 911, 970)
		;

		int typeId;
		int imageResId;
		int colorTypeId;
		String colorName;
		int colorNameMarginLeft;
		int colorSelectorMarginLeft;

		private ExteriorType(int typeId, int imageResId, int colorTypeId, String colorName, int colorNameMarginLeft, int colorSelectorMarginLeft) {
			this.typeId = typeId;
			this.imageResId = imageResId;
			this.colorTypeId = colorTypeId;
			this.colorName = colorName;
			this.colorNameMarginLeft = colorNameMarginLeft;
			this.colorSelectorMarginLeft = colorSelectorMarginLeft;
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_continental_exterior, container, false);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initLayout() {
		try {
			mExteriorImageView = (ImageView) mView.findViewById(R.id.EX_IMAGE_VIEWER);
			mExteriorColorType = (TextView) mView.findViewById(R.id.EX_COLOR_TYPE);
			mExteriorColorName = (TextView) mView.findViewById(R.id.EX_COLOR_NAME);
			mExteriorColorSelector = (View) mView.findViewById(R.id.EX_COLOR_SELECTOR);

			initFonts();

			for (ExteriorType type : ExteriorType.values()) {
				View view = mView.findViewById(type.typeId);
				view.setOnClickListener(this);
			}

			changeExteriorImage(ExteriorType.RES_BV);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initFonts() {
		try {
			SGUtils.setFontToView(((ContinentalActivity)getActivity()).mFontProximaNovaRegular, mExteriorColorType, mExteriorColorName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeExteriorImage(ExteriorType extType) {
		try {
			if(mPrevSelectedType == null) {
				Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), extType.imageResId);
				RecyclingBitmapDrawable currentExtImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

				mExteriorImageView.setImageDrawable(currentExtImage);
				mExteriorColorType.setText(extType.colorTypeId);
				mExteriorColorName.setText(extType.colorName);
			} else {
				TransitionDrawable transitionDrawable = getBitmapDrawableFromType(extType);

				mExteriorImageView.setImageDrawable(transitionDrawable);
				mExteriorColorType.setText(extType.colorTypeId);
				mExteriorColorName.setText(extType.colorName);
			}

			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, SGUtils.dp(getActivity(), 35));
			llp.setMargins(SGUtils.dp(getActivity(), extType.colorNameMarginLeft), 0, 0, SGUtils.dp(getActivity(), 5)); // llp.setMargins(left, top, right, bottom);
			mExteriorColorName.setLayoutParams(llp);

			LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(SGUtils.dp(getActivity(), 52), SGUtils.dp(getActivity(), 4));
			llp2.setMargins(SGUtils.dp(getActivity(), extType.colorSelectorMarginLeft), 0, 0, SGUtils.dp(getActivity(), 2)); // llp.setMargins(left, top, right, bottom);
			mExteriorColorSelector.setLayoutParams(llp2);

			changeColorTab(extType.typeId);

			mPrevSelectedType = extType;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeColorTab(int resId) {
		try {
			for (ExteriorType type : ExteriorType.values()) {
				mView.findViewById(type.typeId).setSelected(false);
			}

			mView.findViewById(resId).setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private TransitionDrawable getBitmapDrawableFromType(ExteriorType extType) {
		try {
			Bitmap prevBitmap = BitmapFactory.decodeResource(getResources(), mPrevSelectedType.imageResId);
			RecyclingBitmapDrawable preExtImage = new RecyclingBitmapDrawable(getResources(), prevBitmap);

			Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), extType.imageResId);
			RecyclingBitmapDrawable currentExtImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

			Drawable[] layers = new Drawable[2];
			layers[0] = new BitmapDrawable(getResources(), preExtImage.getBitmap());
			layers[1] = new BitmapDrawable(getResources(), currentExtImage.getBitmap());

			TransitionDrawable transitionDrawable = new TransitionDrawable(layers);
			transitionDrawable.startTransition(700);

			return transitionDrawable;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
				case R.id.BT_EX_CONTINENTAL_RESERVE_BV :
					changeExteriorImage(ExteriorType.RES_BV);
					break;
				case R.id.BT_EX_CONTINENTAL_RESERVE_PWGM :
					changeExteriorImage(ExteriorType.RES_PWGM);
					break;
				case R.id.BT_EX_CONTINENTAL_RESERVE_MGM :
					changeExteriorImage(ExteriorType.RES_MGM);
					break;
				case R.id.BT_EX_CONTINENTAL_RESERVE_JGM :
					changeExteriorImage(ExteriorType.RES_JGM);
					break;
				case R.id.BT_EX_CONTINENTAL_RESERVE_BVMTC :
					changeExteriorImage(ExteriorType.RES_BVMTC);
					break;
				case R.id.BT_EX_CONTINENTAL_RESERVE_RRMTC :
					changeExteriorImage(ExteriorType.RES_RRMTC);
					break;
				case R.id.BT_EX_CONTINENTAL_RESERVE_MSBM :
					changeExteriorImage(ExteriorType.RES_MSBM);
					break;
				case R.id.BT_EX_CONTINENTAL_COMMON_ISM :
					changeExteriorImage(ExteriorType.COM_ISM);
					break;
				case R.id.BT_EX_CONTINENTAL_COMMON_WPMT :
					changeExteriorImage(ExteriorType.COM_WPMT);
					break;
				case R.id.BT_EX_CONTINENTAL_COMMON_DBM :
					changeExteriorImage(ExteriorType.COM_DBM);
					break;
				case R.id.BT_EX_CONTINENTAL_PRESIDENTIAL_RBM :
					changeExteriorImage(ExteriorType.PRE_RBM);
					break;
				case R.id.BT_EX_CONTINENTAL_PRESIDENTIAL_CECPM :
					changeExteriorImage(ExteriorType.PRE_CECPM);
					break;
				case R.id.BT_EX_CONTINENTAL_PRESIDENTIAL_CCDGPM :
					changeExteriorImage(ExteriorType.PRE_CCDGPM);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
