package com.sgcom.specstand.lincoln.component.detail.fragment.mkz;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.MKZActivity;
import com.sgcom.specstand.lincoln.helper.RecyclingBitmapDrawable;
import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 18..
 */

public class MkzInteriorFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "MkzInteriorFragment";

	private View mView;

	private ImageView mInteriorImageView;
	private TextView mInteriorColorName;
	private View mInteriorColorSelector;

	private InteriorType mPrevSelectedType;

	private enum InteriorType {
		EBONY   (R.id.BT_IN_MKZ_EBONY, R.drawable.mkz_in_ebony, "Ebony", 320, 302),
		TER     (R.id.BT_IN_MKZ_TER, R.drawable.mkz_in_terracotta, "Terracotta", 370, 352),
		CAP     (R.id.BT_IN_MKZ_CAP, R.drawable.mkz_in_cappuccino, "Cappuccino", 420, 402),
		JADE     (R.id.BT_IN_MKZ_JADE, R.drawable.mkz_in_jade, "Jade Grey", 470, 452)
		;

		int typeId;
		int imageResId;
		String colorName;
		int colorNameMarginTop;
		int colorSelectorMarginTop;

		private InteriorType(int typeId, int imageResId, String colorName, int colorNameMarginTop, int colorSelectorMarginTop) {
			this.typeId = typeId;
			this.imageResId = imageResId;
			this.colorName = colorName;
			this.colorNameMarginTop = colorNameMarginTop;
			this.colorSelectorMarginTop = colorSelectorMarginTop;
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_mkz_interior, container, false);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initLayout() {
		try {
			mInteriorImageView = (ImageView) mView.findViewById(R.id.IN_IMAGE_VIEWER);
			mInteriorColorName = (TextView) mView.findViewById(R.id.IN_COLOR_NAME);
			mInteriorColorSelector = (View) mView.findViewById(R.id.IN_COLOR_SELECTOR);

			SGUtils.setFontToView(((MKZActivity)getActivity()).mFontProximaNovaRegular, mInteriorColorName);

			for (InteriorType type : InteriorType.values()) {
				View view = mView.findViewById(type.typeId);
				view.setOnClickListener(this);
			}

			changeInteriorImage(InteriorType.EBONY);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeInteriorImage(InteriorType intType) {
		try {
			if(mPrevSelectedType == null) {
				Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), intType.imageResId);
				RecyclingBitmapDrawable currentExtImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

				mInteriorImageView.setImageDrawable(currentExtImage);
				mInteriorColorName.setText(intType.colorName);
			} else {
				TransitionDrawable transitionDrawable = getBitmapDrawableFromType(intType);

				mInteriorImageView.setImageDrawable(transitionDrawable);
				mInteriorColorName.setText(intType.colorName);
			}

			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			llp.setMargins(0, SGUtils.dp(getActivity(), intType.colorNameMarginTop), 0, 0); // llp.setMargins(left, top, right, bottom);
			mInteriorColorName.setLayoutParams(llp);

			LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(SGUtils.dp(getActivity(), 4), SGUtils.dp(getActivity(), 50));
			llp2.setMargins(SGUtils.dp(getActivity(), 5), SGUtils.dp(getActivity(), intType.colorSelectorMarginTop), SGUtils.dp(getActivity(), 2), 0); // llp.setMargins(left, top, right, bottom);
			mInteriorColorSelector.setLayoutParams(llp2);

			mPrevSelectedType = intType;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private TransitionDrawable getBitmapDrawableFromType(InteriorType intType) {
		try {
			Bitmap prevBitmap = BitmapFactory.decodeResource(getResources(), mPrevSelectedType.imageResId);
			RecyclingBitmapDrawable preInImage = new RecyclingBitmapDrawable(getResources(), prevBitmap);

			Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), intType.imageResId);
			RecyclingBitmapDrawable currentInImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

			Drawable[] layers = new Drawable[2];
			layers[0] = new BitmapDrawable(getResources(), preInImage.getBitmap());
			layers[1] = new BitmapDrawable(getResources(), currentInImage.getBitmap());

			TransitionDrawable transitionDrawable = new TransitionDrawable(layers);
			transitionDrawable.startTransition(700);

			return transitionDrawable;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
				case R.id.BT_IN_MKZ_EBONY :
					changeInteriorImage(InteriorType.EBONY);
					break;
				case R.id.BT_IN_MKZ_CAP :
					changeInteriorImage(InteriorType.CAP);
					break;
				case R.id.BT_IN_MKZ_TER :
					changeInteriorImage(InteriorType.TER);
					break;
				case R.id.BT_IN_MKZ_JADE :
					changeInteriorImage(InteriorType.JADE);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}