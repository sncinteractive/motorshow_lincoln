package com.sgcom.specstand.lincoln.component.detail;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.fragment.mkz.MkzExteriorFragment;
import com.sgcom.specstand.lincoln.component.detail.fragment.mkz.MkzInteriorFragment;
import com.sgcom.specstand.lincoln.component.detail.fragment.mkz.MkzSpecFragment;
import com.sgcom.specstand.lincoln.component.main.SGActivity;
import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class MKZActivity extends SGActivity implements View.OnClickListener {
	private static final String TAG = "MKZActivity";

	private enum TAB_MENU_MKZ {
		SPEC         (R.id.BT_TAB_SPEC),
		EXTERIOR     (R.id.BT_TAB_EXTERIOR),
		INTERIOR     (R.id.BT_TAB_INTERIOR)
		;

		int tabId;

		private TAB_MENU_MKZ(int tabId) {
			this.tabId = tabId;
		}
	}

	private int mCurrentTab;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);

			setContentView(R.layout.activity_mkz);

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initLayout() {
		try {
			initFonts();

			SGUtils.setFontToView(mFontMillerBannerLight, (TextView)findViewById(R.id.CAR_TYPE_TEXT));

			for (TAB_MENU_MKZ menuType: TAB_MENU_MKZ.values()) {
				View view = findViewById(menuType.tabId);
				view.setOnClickListener(this);
			}

			mCurrentTab = R.id.BT_TAB_SPEC;

			findViewById(mCurrentTab).setSelected(true);

			changeFragment(new MkzSpecFragment(), false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeFragment(Fragment frag, boolean withAnim) {
		try {
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			if(withAnim) {
				transaction.setCustomAnimations(R.animator.left_in, R.animator.left_out);
			}

			transaction.replace(R.id.FRAME_VIEW, frag);

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeTab(int resId) {
		try {
			for (TAB_MENU_MKZ menuType: TAB_MENU_MKZ.values()) {
				findViewById(menuType.tabId).setSelected(false);
			}

			findViewById(resId).setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			if(mCurrentTab == v.getId()) {
				return;
			}

			changeTab(v.getId());

			mCurrentTab = v.getId();

			switch (v.getId()) {
				case R.id.BT_TAB_SPEC :
					changeFragment(new MkzSpecFragment(), true);
					break;
				case R.id.BT_TAB_EXTERIOR :
					changeFragment(new MkzExteriorFragment(), true);
					break;
				case R.id.BT_TAB_INTERIOR :
					changeFragment(new MkzInteriorFragment(), true);
					break;
			}
		} catch (Exception e ) {
			e.printStackTrace();
		}
	}
}
