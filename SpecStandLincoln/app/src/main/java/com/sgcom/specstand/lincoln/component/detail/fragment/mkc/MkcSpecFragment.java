package com.sgcom.specstand.lincoln.component.detail.fragment.mkc;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.view.TouchImageView;
import com.sgcom.specstand.lincoln.helper.RecyclingBitmapDrawable;

/**
 * Created by youngmincho on 2017. 3. 18..
 */

public class MkcSpecFragment extends Fragment {
	private static final String TAG = "MkcSpecFragment";

	private View mView;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_mkc_spec, container, false);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initLayout() {
		try {
			TouchImageView specImageView = (TouchImageView) mView.findViewById(R.id.MKC_SPEC_IMAGE_VIEWER);

			Bitmap bitmap = null;
			bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mkc_spec);

			RecyclingBitmapDrawable defaultProfileImage = new RecyclingBitmapDrawable(getResources(), bitmap);
			specImageView.setImageDrawable(defaultProfileImage);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
