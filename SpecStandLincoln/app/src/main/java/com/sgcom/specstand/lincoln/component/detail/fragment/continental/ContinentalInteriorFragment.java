package com.sgcom.specstand.lincoln.component.detail.fragment.continental;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.ContinentalActivity;
import com.sgcom.specstand.lincoln.helper.RecyclingBitmapDrawable;
import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 18..
 */

public class ContinentalInteriorFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "ContinentalInteriorFragment";

	private View mView;

	private ImageView mInteriorImageView;
	private TextView mInteriorColorType;
	private TextView mInteriorColorName;
	private View mInteriorColorSelector;

	private InteriorType mPrevSelectedType;

	private enum InteriorType {
		RES_EBONY          (R.id.BT_IN_CONTINENTAL_RESERVE_EBONY, R.drawable.continental_in_reserve_ebony, R.string.continental_ex_color_type_reserve, "Ebony", 75, 58),
		RES_JG        (R.id.BT_IN_CONTINENTAL_RESERVE_JG, R.drawable.continental_in_reserve_jade_grey, R.string.continental_ex_color_type_reserve, "Jade Gray", 125, 108),
		RES_CAP         (R.id.BT_IN_CONTINENTAL_RESERVE_CAP, R.drawable.continental_in_reserve_cappuccino, R.string.continental_ex_color_type_reserve, "Cappuccino", 175, 159),
		RES_TER         (R.id.BT_IN_CONTINENTAL_RESERVE_TER, R.drawable.continental_in_reserve_terracotta, R.string.continental_ex_color_type_reserve, "Terracotta", 225, 209),
		PRE_RHAPSODY      (R.id.BT_IN_CONTINENTAL_PRESIDENTIAL_RHAPSODY, R.drawable.continental_in_presidential_rhapsody, R.string.continental_ex_color_type_presidential, "Rhapsody\nRhapsody Blue Venetian with Alcantara® inserts\nSilver Mesh Aluminum", 314, 313),
		PRE_CHALET       (R.id.BT_IN_CONTINENTAL_PRESIDENTIAL_CHALET, R.drawable.continental_in_presidential_chalet, R.string.continental_ex_color_type_presidential, "Chalet\nAlpine Venetian\nSilverwood", 366, 365),
		PRE_THOR        (R.id.BT_IN_CONTINENTAL_PRESIDENTIAL_THOROUGHBRED, R.drawable.continental_in_presidential_thoroughbred, R.string.continental_ex_color_type_presidential, "Thoroughbred\nJet Black Venetian\nChilean Maple", 416, 416)
		;

		int typeId;
		int imageResId;
		int colorTypeId;
		String colorName;
		int colorNameMarginTop;
		int colorSelectorMarginTop;

		private InteriorType(int typeId, int imageResId, int colorTypeId, String colorName, int colorNameMarginTop, int colorSelectorMarginTop) {
			this.typeId = typeId;
			this.imageResId = imageResId;
			this.colorTypeId = colorTypeId;
			this.colorName = colorName;
			this.colorNameMarginTop = colorNameMarginTop;
			this.colorSelectorMarginTop = colorSelectorMarginTop;
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_continental_interior, container, false);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initLayout() {
		try {
			mInteriorImageView = (ImageView) mView.findViewById(R.id.IN_IMAGE_VIEWER);
			mInteriorColorType = (TextView) mView.findViewById(R.id.IN_COLOR_TYPE);
			mInteriorColorName = (TextView) mView.findViewById(R.id.IN_COLOR_NAME);
			mInteriorColorSelector = (View) mView.findViewById(R.id.IN_COLOR_SELECTOR);

			initFonts();

			for (InteriorType type : InteriorType.values()) {
				View view = mView.findViewById(type.typeId);
				view.setOnClickListener(this);
			}

			changeInteriorImage(InteriorType.RES_EBONY);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initFonts() {
		try {
			SGUtils.setFontToView(((ContinentalActivity)getActivity()).mFontProximaNovaRegular, mInteriorColorType, mInteriorColorName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeInteriorImage(InteriorType intType) {
		try {
			if(mPrevSelectedType == null) {
				Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), intType.imageResId);
				RecyclingBitmapDrawable currentExtImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

				mInteriorImageView.setImageDrawable(currentExtImage);
				mInteriorColorType.setText(intType.colorTypeId);
				mInteriorColorName.setText(intType.colorName);
			} else {
				TransitionDrawable transitionDrawable = getBitmapDrawableFromType(intType);

				mInteriorImageView.setImageDrawable(transitionDrawable);
				mInteriorColorType.setText(intType.colorTypeId);
				mInteriorColorName.setText(intType.colorName);
			}

			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			llp.setMargins(0, SGUtils.dp(getActivity(), intType.colorNameMarginTop), 0, 0); // llp.setMargins(left, top, right, bottom);
			mInteriorColorName.setLayoutParams(llp);

			LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(SGUtils.dp(getActivity(), 4), SGUtils.dp(getActivity(), 50));
			llp2.setMargins(SGUtils.dp(getActivity(), 5), SGUtils.dp(getActivity(), intType.colorSelectorMarginTop), SGUtils.dp(getActivity(), 2), 0); // llp.setMargins(left, top, right, bottom);
			mInteriorColorSelector.setLayoutParams(llp2);

			mPrevSelectedType = intType;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeAnimation() {
		try {
			if(mInteriorImageView != null) {
				mInteriorImageView.clearAnimation();
				mInteriorImageView = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private TransitionDrawable getBitmapDrawableFromType(InteriorType intType) {
		try {
			Bitmap prevBitmap = BitmapFactory.decodeResource(getResources(), mPrevSelectedType.imageResId);
			RecyclingBitmapDrawable preInImage = new RecyclingBitmapDrawable(getResources(), prevBitmap);

			Bitmap currBitmap = BitmapFactory.decodeResource(getResources(), intType.imageResId);
			RecyclingBitmapDrawable currentInImage = new RecyclingBitmapDrawable(getResources(), currBitmap);

			Drawable[] layers = new Drawable[2];
			layers[0] = new BitmapDrawable(getResources(), preInImage.getBitmap());
			layers[1] = new BitmapDrawable(getResources(), currentInImage.getBitmap());

			TransitionDrawable transitionDrawable = new TransitionDrawable(layers);
			transitionDrawable.startTransition(700);

			return transitionDrawable;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
				case R.id.BT_IN_CONTINENTAL_RESERVE_EBONY :
					changeInteriorImage(InteriorType.RES_EBONY);
					break;
				case R.id.BT_IN_CONTINENTAL_RESERVE_JG :
					changeInteriorImage(InteriorType.RES_JG);
					break;
				case R.id.BT_IN_CONTINENTAL_RESERVE_CAP :
					changeInteriorImage(InteriorType.RES_CAP);
					break;
				case R.id.BT_IN_CONTINENTAL_RESERVE_TER :
					changeInteriorImage(InteriorType.RES_TER);
					break;
				case R.id.BT_IN_CONTINENTAL_PRESIDENTIAL_RHAPSODY :
					changeInteriorImage(InteriorType.PRE_RHAPSODY);
					break;
				case R.id.BT_IN_CONTINENTAL_PRESIDENTIAL_CHALET :
					changeInteriorImage(InteriorType.PRE_CHALET);
					break;
				case R.id.BT_IN_CONTINENTAL_PRESIDENTIAL_THOROUGHBRED :
					changeInteriorImage(InteriorType.PRE_THOR);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		try {
			if(mInteriorImageView != null) {
				mInteriorImageView.clearAnimation();
				mInteriorImageView = null;
			}

			super.onStop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		try {
			if(mInteriorImageView != null) {
				mInteriorImageView.clearAnimation();
				mInteriorImageView = null;
			}

			super.onPause();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroyView() {
		try {
			if(mInteriorImageView != null) {
				mInteriorImageView.clearAnimation();
				mInteriorImageView = null;
			}

			super.onDestroyView();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
