package com.sgcom.specstand.lincoln.component.detail;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.component.detail.fragment.continental.ContinentalExteriorFragment;
import com.sgcom.specstand.lincoln.component.detail.fragment.continental.ContinentalInteriorFragment;
import com.sgcom.specstand.lincoln.component.detail.fragment.continental.ContinentalPresidentialFragment;
import com.sgcom.specstand.lincoln.component.detail.fragment.continental.ContinentalSpecFragment;
import com.sgcom.specstand.lincoln.component.main.SGActivity;
import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class ContinentalActivity extends SGActivity implements View.OnClickListener{
	private static final String TAG = "ContinentalActivity";

	private ContinentalPresidentialFragment mContinentalFragment;
	private ContinentalInteriorFragment mInteriorFragment;

	private enum TAB_MENU_CONTINENTAL {
		PRESIDENTIAL (R.id.BT_TAB_PRESIDENTIAL),
		SPEC         (R.id.BT_TAB_SPEC),
		EXTERIOR     (R.id.BT_TAB_EXTERIOR),
		INTERIOR     (R.id.BT_TAB_INTERIOR)
		;

		int tabId;

		private TAB_MENU_CONTINENTAL(int tabId) {
			this.tabId = tabId;
		}
	}

	private int mCurrentTab;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);

			setContentView(R.layout.activity_continental);

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			initLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initLayout() {
		try {
			initFonts();

			SGUtils.setFontToView(mFontMillerBannerLight, (TextView)findViewById(R.id.CAR_TYPE_TEXT));

			for (TAB_MENU_CONTINENTAL menuType: TAB_MENU_CONTINENTAL.values()) {
				View view = findViewById(menuType.tabId);
				view.setOnClickListener(this);
				view.setSelected(false);
			}

			mCurrentTab = R.id.BT_TAB_PRESIDENTIAL;

			findViewById(mCurrentTab).setSelected(true);

			mContinentalFragment = new ContinentalPresidentialFragment();

			changeFragment(mContinentalFragment, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeFragment(Fragment frag, boolean withAnim) {
		try {
			FragmentTransaction transaction = getFragmentManager().beginTransaction();

			if(withAnim) {
				transaction.setCustomAnimations(R.animator.left_in, R.animator.left_out);
			}

			transaction.replace(R.id.FRAME_VIEW, frag);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeTab(int resId) {
		try {
			for (TAB_MENU_CONTINENTAL menuType: TAB_MENU_CONTINENTAL.values()) {
				findViewById(menuType.tabId).setSelected(false);
			}

			findViewById(resId).setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			if(mCurrentTab == v.getId()) {
				return;
			}

			changeTab(v.getId());

			mCurrentTab = v.getId();

			if(mInteriorFragment != null) {
				mInteriorFragment.removeAnimation();
			}

			switch (v.getId()) {
				case R.id.BT_TAB_PRESIDENTIAL :
					mContinentalFragment = new ContinentalPresidentialFragment();
					changeFragment(mContinentalFragment, true);
					break;
				case R.id.BT_TAB_SPEC :
					changeFragment(new ContinentalSpecFragment(), true);
					break;
				case R.id.BT_TAB_EXTERIOR :
					changeFragment(new ContinentalExteriorFragment(), true);
					break;
				case R.id.BT_TAB_INTERIOR :
					mInteriorFragment = new ContinentalInteriorFragment();
					changeFragment(mInteriorFragment, true);
					break;
			}
		} catch (Exception e ) {
			e.printStackTrace();
		}
	}
}
