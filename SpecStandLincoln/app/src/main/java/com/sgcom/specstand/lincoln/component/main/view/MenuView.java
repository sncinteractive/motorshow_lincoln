package com.sgcom.specstand.lincoln.component.main.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.sgcom.specstand.lincoln.R;
import com.sgcom.specstand.lincoln.helper.PageFactory;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class MenuView extends LinearLayout implements View.OnClickListener{
	private static final String TAG = "MenuView";

	private enum MenuType {
		CONTINENTAL	(R.id.BT_MENU_CONTINENTAL),
		MKX	        (R.id.BT_MENU_MKX),
		MKC		    (R.id.BT_MENU_MKC),
		MKZ			(R.id.BT_MENU_MKZ)
		;

		int menuId;

		private MenuType(int menuId) {
			this.menuId = menuId;
		}
	}

	public MenuView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onFinishInflate() {
		try {
			super.onFinishInflate();

			for (MenuType menuType: MenuType.values()) {
				View view = findViewById(menuType.menuId);
				view.setOnClickListener(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			Context c = getContext();

			Intent intent;

			switch (v.getId()) {
				case R.id.BT_MENU_CONTINENTAL :
					intent = PageFactory.makeContinentalPage(c);
					if (intent != null) {
						c.startActivity(intent);
					}
					break;
				case R.id.BT_MENU_MKX :
					intent = PageFactory.makeMKXPage(c);
					if (intent != null) {
						c.startActivity(intent);
					}
					break;
				case R.id.BT_MENU_MKC :
					intent = PageFactory.makeMKCPage(c);
					if (intent != null) {
						c.startActivity(intent);
					}
					break;
				case R.id.BT_MENU_MKZ :
					intent = PageFactory.makeMKZPage(c);
					if (intent != null) {
						c.startActivity(intent);
					}
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changePage() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
