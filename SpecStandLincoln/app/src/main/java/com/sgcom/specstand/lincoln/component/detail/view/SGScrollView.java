package com.sgcom.specstand.lincoln.component.detail.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import com.sgcom.specstand.lincoln.helper.SGUtils;

/**
 * Created by youngmincho on 2017. 3. 17..
 */

public class SGScrollView extends ScrollView {
	private static final String TAG = "SGScrollView";
	private boolean mIsTop = true;
	private boolean mIsBottom = false;

	private Context mContext;

	public SGScrollView(Context context) {
		super(context);
	}

	public SGScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setContext(Context context) {
		this.mContext = context;
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		try {
			SGUtils.SGLog("error", TAG, "onScrollChanged - scroll top: " + t);
			if (t <= 0) {
				mIsTop = true;
				SGUtils.SGLog("error", TAG, "onScrollChanged - scroll is on top");

				super.onScrollChanged(l, t, oldl, oldt);
				return;
				// reaches the top end
			} else {
				mIsTop = false;
			}

			View view = (View) getChildAt(getChildCount() - 1);
			int diff = (view.getBottom() - (getHeight() + getScrollY() + view.getTop()));// Calculate the scrolldiff
			if (diff <= 0) {
				// if diff is zero, then the bottom has been reached
				SGUtils.SGLog("error", TAG, "onScrollChanged - scroll is on bottom");
				mIsBottom = true;
			} else {
				mIsBottom = false;
			}

			super.onScrollChanged(l, t, oldl, oldt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean ismIsTop() {
		return mIsTop;
	}

	public void setmIsTop(boolean mIsTop) {
		this.mIsTop = mIsTop;
	}

	public boolean ismIsBottom() {
		return mIsBottom;
	}

	public void setmIsBottom(boolean mIsBottom) {
		this.mIsBottom = mIsBottom;
	}
}
