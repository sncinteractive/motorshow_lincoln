package com.sgcom.specstand.lincoln.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.sgcom.specstand.lincoln.assist.SGConfig;

/**
 * Created by youngmincho on 2017. 3. 15..
 */

public class SGUtils {
	public static void SGLog(String _logType, String _tag, String _message) {
		try {
			if(SGConfig.DEBUGGABLE) {
				switch (_logType) {
					case "verbose" :
						Log.v(_tag, _message);
						break;
					case "debug" :
						Log.d(_tag, _message);
						break;
					case "info" :
						Log.i(_tag, _message);
						break;
					case "warning" :
						Log.w(_tag, _message);
						break;
					case "error" :
						Log.e(_tag, _message);
						break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void hideAllSystemUI(Context context) {
		try {
			final int systemUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE;

			final View decorView = ((Activity)context).getWindow().getDecorView();
			decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
				@Override
				public void onSystemUiVisibilityChange(int visibility) {
					try {
						if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
							decorView.setSystemUiVisibility(systemUIFlag);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			decorView.setSystemUiVisibility(systemUIFlag);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int dp(Context context, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}

	public static void setFontToView(Typeface _type_face, TextView... views) {
		for (TextView view : views) {
			view.setTypeface(_type_face);
		}
	}
}
